<?php

/**
 * Wrapper function for du_sub_navigation class
 */
function du_sub_nav( $class_name='', $title=true, $exclude=array(), $echo='show' ) {
  $sub_navigation = new du_sub_navigation( $class_name, $title, $exclude, $echo );
  if ( $sub_navigation->echo == 'show' ) {
    echo $sub_navigation->nav_string;
  } else {
    return $sub_navigation->nav_string;
  }
}

function du_sub_nav_exists(){
  $sub_navigation = new du_sub_navigation( '', false, array(), false );
  return $sub_navigation->exists();
}

class du_sub_navigation {

/**
 * *******
 * FILTERS
 * *******
 * 
 * du_sub_nav_title   filters title output
 * 
 */

  public function __construct( $class_name, $show_title, $exclude, $echo ) {

    /**
     * Grab current post object and store in in a public variable
     */
    global $post; 
    $this->post = $post;

    /**
     * If a class name is set, then use it.  Otherwise, use the default
     * class name of current-sub-nav
     */
    $this->class_name = ( !empty( $class_name ) ? $class_name : 'current-sub-nav' );
 
    /**
     * Grab the excluded ids array
     */
    $this->exclude = $exclude;

    /**
     * $children_pages = children of current page
     */
    $this->children_pages = ( !empty( $post ) ? wp_list_pages(
      'child_of=' . $this->post->ID . '&sort_column=menu_order&depth=2&title_li=&echo=0'
    ) : '' );

    /**
     * Display header over nav
     */
    $this->show_title = $show_title;

    /**
     * Should we return or echo result
     * Default: true (echo)
     */
    $this->echo = $echo;
    
    /**
     * Create variable to house nav
     */
    $this->nav_string = '';

    /**
     * $same_level_pages = sibling pages of current page
     */
    $this->same_level_pages = ( !empty( $post ) ? wp_list_pages(
      'child_of=' . $this->post->post_parent . '&sort_column=menu_order&depth=2&title_li=&echo=0' 
    ) : '' );

    if ( $this->exists() ) :

      $this->build_header();
      $this->build_menu();

    endif; 
  }

  function exists() {
    if ( $this->post ) :
      if ( ( $this->post->post_parent != 0 || 
        !empty( $this->children_pages ) ) 
        && ( !in_array( $this->post->post_parent, $this->exclude ) )
        && ( !get_search_query() ) )
        return true;
    endif;
  }

  function build_header() {
    /**
     * The display title will either be the current page's title if this page has children
     * or the parent page if this page does not have children.
     */
    $this->display_title = ( !empty( $this->children_pages ) ? get_the_title( $this->post->ID ) : get_the_title( $this->post->post_parent ) );
    $this->display_title = apply_filters( 'du_sub_nav_title', $this->display_title );
    
    if ( $this->display_title == true && $this->show_title == true )
      $this->nav_string .= '<header class="' . $this->class_name . '-title">' . $this->display_title . '</header>';
  }

  function build_menu() {
    $this->nav_string .= '<nav class="subpage-navigation ' . $this->class_name . '">' . "\n"
      .   '   <ul>' . "\n"
      .   '   ' . ( empty( $this->children_pages ) ? $this->same_level_pages : $this->children_pages )
      .   '   </ul>' . "\n"
      .   '</nav>';
  }

}